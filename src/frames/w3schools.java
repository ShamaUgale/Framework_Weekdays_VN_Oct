package frames;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class w3schools {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		
		
System.setProperty("webdriver.chrome.driver", "C:\\Users\\shama.ugale\\Downloads\\chromedriver_win32 (3)\\chromedriver.exe");
		
		ChromeDriver driver= new ChromeDriver();
		driver.get("http://www.w3schools.com/html/tryit.asp?filename=tryhtml_default");
		
		
		System.out.println("No of frames : " + driver.findElements(By.tagName("iframe")).size());
		
		// using the index 
//		driver.switchTo().frame(2);
		
		//using id or name
		
//		driver.switchTo().frame("iframeResult");
		
		//by xpath - webelement
		WebElement frame=driver.findElement(By.xpath("//*[@id='iframeResult']"));
		driver.switchTo().frame(frame);
		
		// this is in fframe
		String txt= driver.findElement(By.xpath("/html/body/h1")).getText();
		
		System.out.println(txt);
		
	}

}
