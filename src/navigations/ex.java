package navigations;

import org.openqa.selenium.chrome.ChromeDriver;

public class ex {

	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "C:\\Users\\shama.ugale\\Downloads\\chromedriver_win32 (3)\\chromedriver.exe");
		
		ChromeDriver driver= new ChromeDriver();
		
		//way 1
		// nav to a url
		driver.get("http://www.google.com");
		
		
		// way 2
		driver.navigate().to("http://www.facebook.com");
		
		driver.navigate().back();
		Thread.sleep(2000);
		driver.navigate().forward();
		Thread.sleep(2000);
		driver.navigate().refresh();
		
		driver.close();
	}

}
