package alterts;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ok {

	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {

		
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\shama.ugale\\Downloads\\chromedriver_win32 (3)\\chromedriver.exe");
		
		ChromeDriver driver= new ChromeDriver();
		driver.get("http://www.javascriptkit.com/javatutors/alert2.shtml");
		
		WebElement btn1= driver.findElement(By.name("B2"));
		btn1.click();
		
		
		Alert alt= driver.switchTo().alert();
		String msg=alt.getText();
		Thread.sleep(2000);
		alt.accept(); // clicks on OK/Yes button
		Thread.sleep(2000);
		driver.close();
	}

}
