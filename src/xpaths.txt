///////////////  general format //////////////////


//tagname[@attributename='its value'  and/or  @attributename='its value']

//*[@id="container"]/div/header/div[1]/div[2]/div/div/div[2]/form/div/div[2]/button

//button[@class='vh79eN']
//a[@href='/viewcart']


////////////////////// to identify based on the text //////////////

//tagname[text()='the text']


////////////////////////// dynamic ids //////////////////////////

//tagname[starts-with(@id,'fixed pattern at the start')]
//tagname[contains(@id,'fixed text')]